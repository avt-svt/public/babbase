
add_executable(TEST_BABBASE ${PROJECT_SOURCE_DIR}/test/testBranchAndBoundBase.cpp)
find_package(Boost  COMPONENTS    unit_test_framework	  REQUIRED)

# add directories that contain important header files
target_include_directories(TEST_BABBASE
	PRIVATE
		${Boost_INCLUDE_DIRS} # directory containing boost headers
		${PROJECT_SOURCE_DIR}/test
   )
target_link_libraries(TEST_BABBASE
  babbase
  rapidcheck
  rapidcheck_boost_test
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
)
# indicates the shared library variant
target_compile_definitions(TEST_BABBASE PUBLIC "BOOST_TEST_DYN_LINK=1")

target_compile_options(TEST_BABBASE
	PUBLIC
		# Disable a warning caused by rapidcheck (cannot append it to rapidcheck target here)
		$<$<CXX_COMPILER_ID:GNU>: -Wno-narrowing>
)

add_subdirectory(${PROJECT_SOURCE_DIR}/dep/rapidcheck )
add_subdirectory(${PROJECT_SOURCE_DIR}/dep/rapidcheck/extras/boost_test)

add_test(TEST_BABBASE ${CMAKE_BINARY_DIR}/TEST_BABBASE) #ADD OPTIONS HERE

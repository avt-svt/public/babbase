/**********************************************************************************
 * Copyright (c) 2019-2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#include "babBrancher.h"
#include "babNode.h"

#include <iostream>
#include <numeric>
#include <tuple>


/////////////////////////////////////////////////////////////////////////////////////////////
// An example for an custom node scoring function. Such functions would be implmented in the client code.
double
custom_node_scoring(const babBase::BabNode& node, std::vector<babBase::OptimizationVariable> vars)
{
    std::vector<double> lbs  = node.get_lower_bounds();
    std::vector<double> ubs  = node.get_upper_bounds();
    double sumLower          = std::accumulate(lbs.begin(), lbs.end(), /*starting at*/ 0.0);
    double sumUpper          = std::accumulate(ubs.begin(), ubs.end(), /*starting at*/ 0.0);
    double sumOfBoundsWidths = (sumUpper - sumLower);
    return sumOfBoundsWidths;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// Sets up optimization variables and root node
std::pair<std::vector<babBase::OptimizationVariable>, babBase::BabNode>
create_variable_and_node()
{
    // Very simple setup for testing
    // Set up optimization variables
    std::vector<babBase::OptimizationVariable> optitest;
    babBase::Bounds boundsTest(0.5, 15.5);
    babBase::OptimizationVariable a(boundsTest);
    optitest.emplace_back(a);
    optitest.emplace_back(a);

    // Setup root node
    babBase::BabNode test_root(0.0 /*Pruning Score*/, optitest, 0 /*data set index*/, 0 /*ID*/, 0 /*depth*/, false /*do not augment data set*/);
    return std::make_pair(optitest, test_root);
}


/////////////////////////////////////////////////////////////////////////////////////////////
// Main function of test
int
main(int argc, char** argv)
{

    std::cout << "Starting Simple Test " << std::endl;

    // Setup
    std::vector<babBase::OptimizationVariable> optitest;
    babBase::BabNode root;
    std::tie(optitest, root) = create_variable_and_node();

    // Example
    babBase::Brancher ba(optitest);
    ba.set_node_selection_score_function(custom_node_scoring);
    ba.branch_on_node(root, std::vector<double>{3.0, 3.0}, 2.0, 0);
    std::cout << ba.get_nodes_in_tree() << std::endl;
    int counter = 1;

    babBase::BabNode nodeToProcess = ba.get_next_node();    // Should maybe be cached inside
    // Processing ... yields:
    double relaxationObjFunction = 10;
    double pruningValue          = relaxationObjFunction;
    std::vector<double> relaxationPoint{3.0, 1.5};
    // Maybe changed lower bounds

    babBase::BabNode nodeAfterProcess(pruningValue, nodeToProcess.get_lower_bounds(), nodeToProcess.get_upper_bounds(), 0 /*data set index*/, 1 /*ID*/, 0 /*depth*/, false /*do not augment data set*/);

    ba.register_node_change(nodeToProcess.get_ID(), nodeAfterProcess);
    ba.branch_on_node(nodeAfterProcess, relaxationPoint, relaxationObjFunction, 0);

    std::cout << ba.get_nodes_in_tree() << std::endl;
    std::cout << "Looks good..." << std::endl;

    return 0;
}
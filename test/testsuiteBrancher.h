/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * @file testsuiteBrancher.h
 *
 **********************************************************************************/

#pragma once

#include "RapidCheckUtils.h"
#include "babBrancher.h"
#include "babException.h"

#include <ciso646>    //enable and and or in Visual Studio
#include <numeric>
#include <stdio.h>
#include <tuple>

#include <ciso646>    //enable and and or in Visual Studio
#include <numeric>
#include <stdio.h>
#include <tuple>

double
custom_node_scoring(const babBase::BabNode& node, std::vector<babBase::OptimizationVariable> vars)
{
    std::vector<double> lbs  = node.get_lower_bounds();
    std::vector<double> ubs  = node.get_upper_bounds();
    double sumLower          = std::accumulate(lbs.begin(), lbs.end(), /*starting at*/ 0.0);
    double sumUpper          = std::accumulate(ubs.begin(), ubs.end(), /*starting at*/ 0.0);
    double averageBondsWidth = (sumUpper - sumLower);
    return averageBondsWidth;
}


RC_BOOST_PROP(on_default_returns_highest_node_selection_node_first, (const rc::NodesAndVariablesFixture& fixture))
{
    const auto vars = fixture.vars;
    auto nodes      = fixture.nodes;
    babBase::Brancher brancher(vars);
    for (const babBase::BabNode& currentNode : nodes) {
        brancher.insert_root_node(currentNode);
    }
    std::sort(nodes.begin(), nodes.end(), [](const babBase::BabNode nodea, const babBase::BabNode nodeb) { return nodea.get_pruning_score() < nodeb.get_pruning_score(); });
    for (const babBase::BabNode& expectedNode : nodes) {
        babBase::BabNode returnedNode = brancher.get_next_node();
        RC_ASSERT(returnedNode.get_pruning_score() == expectedNode.get_pruning_score());
    }
}


RC_BOOST_PROP(node_with_lowest_custom_scoring_function_is_returned_first, (const rc::NodesAndVariablesFixture& fixture))
{
    auto nodes = fixture.nodes;
    auto vars  = fixture.vars;
    babBase::Brancher brancher(vars);

    brancher.set_node_selection_score_function(custom_node_scoring);
    for (const babBase::BabNode& currentNode : nodes) {
        brancher.insert_root_node(currentNode);
    }
    //sort decending
    std::sort(nodes.begin(), nodes.end(), [&vars](const babBase::BabNode nodea, const babBase::BabNode nodeb) { return custom_node_scoring(nodea, vars) > custom_node_scoring(nodeb, vars); });
    for (const babBase::BabNode& expectedNode : nodes) {
        babBase::BabNode returnedNode = brancher.get_next_node();
        RC_ASSERT(custom_node_scoring(returnedNode, vars) == custom_node_scoring(expectedNode, vars));
    }
}


RC_BOOST_PROP(register_node_change_accepts_correct_ids_in_any_order_ignores_id_saved_in_node, (const rc::NodesAndVariablesFixture& fixture))
{
    auto nodes = fixture.nodes;
    auto vars  = fixture.vars;

    babBase::Brancher brancher(vars);
    for (auto currentNode : nodes) {
        brancher.insert_root_node(currentNode);
    }
    std::vector<babBase::BabNode> returnedNodes;
    for (int i = 0; i < fixture.size; ++i) {
        returnedNodes.push_back(brancher.get_next_node());
    }
    // permutate the returnedNodes vector
    for (int i = 1; i < 4; i++) {
        std::next_permutation(returnedNodes.begin(), returnedNodes.end(), [](babBase::BabNode a, babBase::BabNode b) { return a.get_ID() > b.get_ID(); });
    }
    for (auto returnedNode : returnedNodes) {
        //should ignore ID of second argument
        //should not throw just because diffenent ordering
        babBase::BabNode returnedNodeWithChangedId(returnedNode.get_pruning_score(), returnedNode.get_lower_bounds(), returnedNode.get_upper_bounds(), returnedNode.get_index_dataset(), returnedNode.get_ID() + 6, returnedNode.get_depth(), returnedNode.get_augment_data());
        brancher.register_node_change(returnedNode.get_ID(), returnedNodeWithChangedId);
    }

    RC_SUCCEED("Register node did not throw when called out of order");
}


RC_BOOST_PROP(register_node_change_throws_on_missing_id, (const babBase::BabNode randomNode))
{
    auto vars = *rc::gen::container<std::vector<babBase::OptimizationVariable>>(randomNode.get_upper_bounds().size(), rc::gen::arbitrary<babBase::OptimizationVariable>());
    babBase::Brancher brancher(vars);
    brancher.insert_root_node(randomNode);
    babBase::BabNode retNode = brancher.get_next_node();
    //try correct id
    brancher.register_node_change(retNode.get_ID(), retNode);
    //try id again (should throw)
    RC_ASSERT_THROWS_AS(brancher.register_node_change(retNode.get_ID(), retNode), babBase::BranchAndBoundBaseException);
}


RC_BOOST_PROP(branch_on_node_increases_nodes_in_tree_by_two_execept_for_fixed_parent_nodes, (const std::vector<babBase::OptimizationVariable>& optitest))
{
    RC_PRE(!optitest.empty());    // Test only valid if vector is not empty
    auto root = *rc::Arbitrary<babBase::BabNode>::sized(optitest.size());
    //Example
    babBase::Brancher ba(optitest);
    ba.set_node_selection_score_function(custom_node_scoring);

    //create a valid solutionPoint
    std::vector<double> midpoint;
    midpoint.reserve(root.get_upper_bounds().size());
    std::vector<double> upper(root.get_upper_bounds());
    std::vector<double> lower(root.get_lower_bounds());
    std::transform(upper.begin(), upper.end(), lower.begin(), std::back_inserter(midpoint), [](double a, double b) { return (a + b) * 0.5; });


    bool isFixed, pseudoFixed;
    std::tie(isFixed, pseudoFixed) = ba.branch_on_node(root, midpoint, 2.0, /*relativeTolerance*/ 0.1);


    if (!isFixed && !pseudoFixed) {
        RC_ASSERT((ba.get_nodes_in_tree() == 2));
        babBase::BabNode firstChild  = ba.get_next_node();
        babBase::BabNode secondChild = ba.get_next_node();
    }
    else if (isFixed) {
        RC_ASSERT((ba.get_nodes_in_tree() == 1));
    }
    else if (pseudoFixed) {
        RC_ASSERT((ba.get_nodes_in_tree() == 0));
    }
    if (ba.get_nodes_in_tree() > 0) {
        babBase::BabNode nodeToProcess = ba.get_next_node();    // should maybe be cached inside
    }
}


RC_BOOST_PROP(get_next_node_throws_when_no_node_availble, (const rc::NodesAndVariablesFixture& fixture))
{
    auto nodes = fixture.nodes;
    auto vars  = fixture.vars;
    babBase::Brancher brancher(vars);

    //fill tree
    for (babBase::BabNode node : nodes) {
        brancher.insert_root_node(node);
    }
    //empty the tree again
    for (babBase::BabNode node : nodes) {
        brancher.get_next_node();
    }
    RC_ASSERT_THROWS_AS(brancher.get_next_node(), babBase::BranchAndBoundBaseException);
}


RC_BOOST_PROP(brancher_fathoms_nodes_exceeding_pruning_threshold_on_adding, (const babBase::BabNode& node))
{
    auto vars = *rc::gen::container<std::vector<babBase::OptimizationVariable>>(node.get_upper_bounds().size(), rc::gen::arbitrary<babBase::OptimizationVariable>());
    babBase::Brancher brancher(vars);

    //create a valid solutionPoint
    std::vector<double> midpoint;
    midpoint.reserve(node.get_upper_bounds().size());
    std::vector<double> upper(node.get_upper_bounds());
    std::vector<double> lower(node.get_lower_bounds());
    std::transform(upper.begin(), upper.end(), lower.begin(), std::back_inserter(midpoint), [](double a, double b) { return (a + b) * 0.5; });


    brancher.decrease_pruning_score_threshold_to(node.get_pruning_score() - 0.01);    // -> node should be fathomed
    unsigned int dummy = 0;
    brancher.branch_on_node(node, midpoint, 0.012, dummy);
    RC_ASSERT((brancher.get_nodes_in_tree() == 0));

    brancher = babBase::Brancher(vars);
    brancher.enable_pruning_with_rel_and_abs_tolerance(0.0, /*absTol*/ 1.0);
    brancher.decrease_pruning_score_threshold_to(node.get_pruning_score() + 0.9);    //pruning score of node will be lower than threshold, but within tolerance to be fathomed
    brancher.branch_on_node(node, midpoint, 0.1245, dummy);
    RC_ASSERT((brancher.get_nodes_in_tree() == 0));
}


BOOST_AUTO_TEST_CASE(brancher_combined_test_including_absdiam_reldiam_strategy_switch)
{

    unsigned arbitraryDepth = 2;
    babBase::OptimizationVariable var1(babBase::Bounds(-1.0, 1.0), babBase::enums::VT_CONTINUOUS, true, "var1");
    babBase::OptimizationVariable var2(babBase::Bounds(-2.0, 2.0), babBase::enums::VT_CONTINUOUS, true, "var2");
    std::vector<babBase::OptimizationVariable> vars = {var1, var2};
    babBase::Brancher brancher(vars);
    brancher.set_node_selection_score_function(babBase::low_pruning_score_first);

    std::vector<double> lowerAllNodes = {0, 0};

    std::vector<double> upperSmallNodes = {0.2, 0.2};
    babBase::BabNode nodeToKeep(-1.2 /*dont change*/, lowerAllNodes, upperSmallNodes, 0, 0, arbitraryDepth, false);
    babBase::BabNode nodeToFathom(20.0, lowerAllNodes, upperSmallNodes, 0, 0, arbitraryDepth, false);

    std::vector<double> upperBiggestAbsolute = {0.5, 1.0};
    babBase::BabNode biggestAbsolute(-2.0, lowerAllNodes, upperBiggestAbsolute, 0, 0, arbitraryDepth, false);

    std::vector<double> upperBiggestRelative = {0.7, 1.0};
    babBase::BabNode biggestRelative(-1.0, lowerAllNodes, upperBiggestRelative, 0, 0, arbitraryDepth, false);

    std::vector<double> upperSetFixed = {0.001, 0.001};
    babBase::BabNode toSetFixed(0.0, lowerAllNodes, upperSetFixed, 0, 0, arbitraryDepth, false);

    brancher.insert_root_node(nodeToKeep);
    brancher.insert_root_node(nodeToFathom);


    //test if fathoming works
    unsigned int nodesBefore = brancher.get_nodes_in_tree();
    // should fathom nodeToFathom
    brancher.decrease_pruning_score_threshold_to(10);
    BOOST_CHECK(brancher.get_nodes_in_tree() == (nodesBefore - 1));

    //test if hasIncumbent is set to false on set_new_incumbent
    babBase::BabNode returnedNode = brancher.get_next_node();    //should be nodeToKeep
    BOOST_CHECK_EQUAL(returnedNode.get_pruning_score(), -1.2);

    //registering should  not throw on valid id (gotten from get_next_node)
    BOOST_CHECK_NO_THROW(brancher.register_node_change(returnedNode.get_ID(), returnedNode));
    //now id is invalid so repetition should lead to throwin
    BOOST_CHECK_THROW(brancher.register_node_change(returnedNode.get_ID(), returnedNode), babBase::BranchAndBoundBaseException);

    //check if branch_on_node considers  nodes inside tolerance fixed and ignores them
    bool fixed, consideredFixed;
    unsigned int dummy = 0;
    nodesBefore        = brancher.get_nodes_in_tree();

    std::tie(fixed, consideredFixed) = brancher.branch_on_node(toSetFixed, upperBiggestAbsolute, 1.0, 0.1);
    BOOST_CHECK(consideredFixed);
    BOOST_CHECK(!fixed);
    BOOST_CHECK(brancher.get_nodes_in_tree() == (nodesBefore));

    //check ABSDIAM strategy
    brancher.set_branching_dimension_selection_strategy(babBase::enums::BV_ABSDIAM);
    nodesBefore = brancher.get_nodes_in_tree();
    brancher.branch_on_node(biggestAbsolute, upperBiggestAbsolute, 1.0);
    BOOST_CHECK(brancher.get_nodes_in_tree() == (nodesBefore + 2));
    //check if ABSDIAM was used
    for (int i = 0; i < 2; i++) {
        returnedNode = brancher.get_next_node();
        //should have branched on second variable
        bool firstVarBoundsChanged  = (returnedNode.get_lower_bounds().at(0) != biggestAbsolute.get_lower_bounds().at(0)) || (returnedNode.get_upper_bounds().at(0) != biggestAbsolute.get_upper_bounds().at(0));
        bool secondVarBoundsChanged = (returnedNode.get_lower_bounds().at(1) != biggestAbsolute.get_lower_bounds().at(1)) || (returnedNode.get_upper_bounds().at(1) != biggestAbsolute.get_upper_bounds().at(1));
        BOOST_CHECK((!firstVarBoundsChanged and secondVarBoundsChanged));
    }

    //check RELDIAM strategy
    brancher.set_branching_dimension_selection_strategy(babBase::enums::BV_RELDIAM);
    nodesBefore = brancher.get_nodes_in_tree();
    brancher.branch_on_node(biggestRelative, upperBiggestAbsolute, 1.0);
    BOOST_CHECK(brancher.get_nodes_in_tree() == (nodesBefore + 2));
    //check if RELDIAM was used
    for (int i = 0; i < 2; i++) {
        returnedNode = brancher.get_next_node();
        //should have branched on first variable
        bool firstVarBoundsChanged  = (returnedNode.get_lower_bounds().at(0) != biggestRelative.get_lower_bounds().at(0)) || (returnedNode.get_upper_bounds().at(0) != biggestRelative.get_upper_bounds().at(0));
        bool secondVarBoundsChanged = (returnedNode.get_lower_bounds().at(1) != biggestRelative.get_lower_bounds().at(1)) || (returnedNode.get_upper_bounds().at(1) != biggestRelative.get_upper_bounds().at(1));
        BOOST_CHECK((firstVarBoundsChanged and !secondVarBoundsChanged));
    }
}
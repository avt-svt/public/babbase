/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * @file testsuiteBabTree.h
 *
 **********************************************************************************/

#pragma once

#include <algorithm>
#include <numeric>
#include <stdio.h>
#include <tuple>
#include <vector>

#include "RapidCheckUtils.h"
#include "babException.h"
#include "babTree.h"


RC_BOOST_PROP(tree_gives_unique_ids, ())
{
    babBase::BabTree tree;
    auto times = *rc::gen::inRange(2, 50);
    std::vector<unsigned int> returns;
    returns.reserve(times);
    for (int i = 0; i < times; i++) {
        unsigned int id = tree.get_valid_id();
        returns.push_back(id);
    }
    std::sort(returns.begin(), returns.end());
    auto it               = std::unique(returns.begin(), returns.end());
    bool uniqueIdReturned = (it == returns.end());
    RC_ASSERT(uniqueIdReturned);
}


RC_BOOST_PROP(tree_on_default_returns_highest_node_selection_node_first, ())
{
    const auto nodes = *rc::gen::container<std::vector<babBase::BabNode>>(rc::gen::arbitrary<babBase::BabNode>());
    auto scores      = *rc::gen::container<std::vector<double>>(nodes.size(), rc::gen::arbitrary<double>());
    babBase::BabTree tree;
    std::vector<double>::const_iterator scoreIt = scores.begin();
    for (const babBase::BabNode& node : nodes) {
        tree.add_node(babBase::BabNodeWithInfo(node, *(scoreIt++)));
    }
    std::sort(scores.begin(), scores.end(), std::greater<double>());
    for (double expectedSortedScore : scores) {
        babBase::BabNodeWithInfo node = tree.pop_next_node();
        RC_ASSERT(node.get_node_selection_score() == expectedSortedScore);
    }
}


RC_BOOST_PROP(tree_searches_breadth_first_if_selected_and_ids_are_from_tree, ())
{
    const auto nodes = *rc::gen::container<std::vector<babBase::BabNode>>(rc::gen::arbitrary<babBase::BabNode>());
    auto scores      = *rc::gen::container<std::vector<double>>(nodes.size(), rc::gen::arbitrary<double>());
    babBase::BabTree tree;

    //supress warning
    std::cerr.setstate(std::ios_base::failbit);
    tree.set_node_selection_strategy(babBase::enums::NS_BREADTHFIRST);
    std::cerr.clear();
    std::vector<double>::const_iterator scoreIt = scores.begin();
    for (const babBase::BabNode& node : nodes) {
        unsigned int id                    = tree.get_valid_id();
        babBase::BabNode nodeWithCorrectId = babBase::BabNode(node.get_pruning_score(), node.get_lower_bounds(), node.get_upper_bounds(), node.get_index_dataset(), id, node.get_depth(), node.get_augment_data());
        tree.add_node(babBase::BabNodeWithInfo(nodeWithCorrectId, *(scoreIt++)));
    }
    for (babBase::BabNode expectedNode : nodes) {
        babBase::BabNode returnedNode = tree.pop_next_node();
        RC_ASSERT(returnedNode.get_pruning_score() == expectedNode.get_pruning_score());
    }
}


RC_BOOST_PROP(tree_searches_depth_first_if_selected_and_ids_are_from_tree, ())
{
    auto nodes  = *rc::gen::container<std::vector<babBase::BabNode>>(rc::gen::arbitrary<babBase::BabNode>());
    auto scores = *rc::gen::container<std::vector<double>>(nodes.size(), rc::gen::arbitrary<double>());
    babBase::BabTree tree;

    //supress warning
    std::cerr.setstate(std::ios_base::failbit);
    tree.set_node_selection_strategy(babBase::enums::NS_DEPTHFIRST);
    std::cerr.clear();
    std::vector<double>::const_iterator scoreIt = scores.begin();
    for (const babBase::BabNode& node : nodes) {
        unsigned int id                    = tree.get_valid_id();
        babBase::BabNode nodeWithCorrectId = babBase::BabNode(node.get_pruning_score(), node.get_lower_bounds(), node.get_upper_bounds(), node.get_index_dataset(), id, node.get_depth(), node.get_augment_data());
        tree.add_node(babBase::BabNodeWithInfo(nodeWithCorrectId, *(scoreIt++)));
    }
    std::reverse(nodes.begin(), nodes.end());
    for (babBase::BabNode expectedNode : nodes) {
        babBase::BabNode returnedNode = tree.pop_next_node();
        RC_ASSERT(returnedNode.get_pruning_score() == expectedNode.get_pruning_score());
    }
}


RC_BOOST_PROP(tree_coniders_tolerance_when_pruning, ())
{
    babBase::BabTree tree;
    const auto nodes  = *rc::gen::nonEmpty(rc::Arbitrary<std::vector<babBase::BabNode>>::sized(1));
    const auto scores = *rc::gen::container<std::vector<double>>(nodes.size(), rc::gen::arbitrary<double>());
    auto scoreIt      = scores.begin();
    for (const babBase::BabNode& node : nodes) {
        tree.add_node(babBase::BabNodeWithInfo(node, *(scoreIt++)));
    }
    babBase::BabTree save = tree;
    double lowest         = tree.get_lowest_pruning_score();

    //abs tolerance is not (much) larger than specified
    tree.enable_pruning_with_rel_and_abs_tolerance(0.0, 0.9 * std::abs(lowest) + 0.1);
    tree.set_pruning_score_threshold(lowest + 1.0 * std::abs(lowest) + 0.11);
    RC_ASSERT((tree.get_nodes_left() > 0));

    //abs tolerance is not (much) smaller than specified
    tree = save;
    tree.enable_pruning_with_rel_and_abs_tolerance(0.0, 1.1 * std::abs(lowest) + 0.1);
    tree.set_pruning_score_threshold(tree.get_lowest_pruning_score() + 1.0 * std::abs(lowest) + 0.1);
    RC_ASSERT((tree.get_nodes_left() == 0));

    //hard to test relative for 0, so in that case stop here
    RC_SUCCEED_IF((save.get_lowest_pruning_score() == 0.0));


    //node with smallest pruning score should be keept in tree
    double newTolerance      = 0.5;
    lowest                   = save.get_lowest_pruning_score();
    double criticalThreshold = std::max(lowest / (1 - newTolerance), lowest / (1 + newTolerance));
    save.enable_pruning_with_rel_and_abs_tolerance(newTolerance, 0.0);
    save.set_pruning_score_threshold(criticalThreshold + 0.01 * std::abs(criticalThreshold));
    RC_ASSERT((save.get_nodes_left() > 0));


    //relative threshold is so large that even lowest pruning score is fathomed
    save.enable_pruning_with_rel_and_abs_tolerance(newTolerance, 0.0);
    save.set_pruning_score_threshold(criticalThreshold - 0.01 * std::abs(criticalThreshold));
    RC_ASSERT((save.get_nodes_left() == 0));
}


BOOST_AUTO_TEST_CASE(empty_tree_returns_infinity_for_lowest_pruning_score)
{
    babBase::BabTree tree;
    tree.set_pruning_score_threshold(0.0);    //set threshold from inf to 0.0 to make proper test
    BOOST_TEST(std::isinf(tree.get_lowest_pruning_score()));
    BOOST_TEST(tree.get_lowest_pruning_score() > 0.0);
    BOOST_TEST(std::isinf(tree.get_pruning_score_gap()));
    BOOST_TEST(tree.get_pruning_score_gap() < 0.0);
    babBase::BabTree tree2;
    std::vector<double> vec1 = {1.0, 1.0};
    babBase::BabNode testnode(1.0, vec1, vec1, 0, 0, 1, false);
    tree2.add_node(babBase::BabNodeWithInfo(testnode, 1.0));
    BOOST_TEST(std::isinf(tree.get_pruning_score_gap()));
    BOOST_TEST(tree.get_pruning_score_gap() < 0.0);
}


BOOST_AUTO_TEST_CASE(empty_tree_may_only_throw_on_pop_node)
{
    babBase::BabTree tree;
    BOOST_CHECK_NO_THROW(tree.get_nodes_left());
    BOOST_CHECK_NO_THROW(tree.get_valid_id());
    BOOST_CHECK_NO_THROW(tree.get_pruning_score_gap());
    BOOST_CHECK_NO_THROW(tree.get_lowest_pruning_score());
    BOOST_CHECK_NO_THROW(tree.enable_pruning_with_rel_and_abs_tolerance(0.1, 0.1));
    BOOST_CHECK_NO_THROW(tree.set_pruning_score_threshold(0.0));
    //BOOST_WARN_THROW(tree.pop_next_node());
    BOOST_CHECK_NO_THROW(tree.add_node(babBase::BabNodeWithInfo(babBase::BabNode(), 0.5)));
}
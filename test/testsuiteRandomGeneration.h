/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * @file testsuiteBrancher.h
 *
 **********************************************************************************/
 
#include "RapidCheckUtils.h"

BOOST_AUTO_TEST_CASE(RandomGenerationTests)
{
    bool first_check  = rc::check("RandomGenerationTest: UpperBounds are always bigger than lower bounds",
                                 [](const babBase::Bounds &bound) { return bound.lower <= bound.upper; });
    bool second_check = rc::check("RandomGenerationTest: UpperBounds are always bigger than lower bounds in optimization variables",
                                  [](const babBase::OptimizationVariable &optVar) { return optVar.get_lower_bound() <= optVar.get_upper_bound(); });
    BOOST_CHECK((first_check && second_check));
}
/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * @file RapidCheckUtils.h
 *
 **********************************************************************************/

#pragma once

#include <rapidcheck.h>
#include <rapidcheck/boost_test.h>

#include "babNode.h"
#include "babOptVar.h"
#include "babUtils.h"


namespace rc {


//enable random generation of Bounds
template <>
struct Arbitrary<babBase::Bounds> {
    static Gen<babBase::Bounds> arbitrary()
    {
        double gap   = *gen::arbitrary<double>();
        double lower = *gen::arbitrary<double>();
        return gen::construct<babBase::Bounds>(gen::just(lower), gen::just(lower + std::abs(gap)));
    }
};
//enable random generation of OptimizationVariables
template <>
struct Arbitrary<babBase::OptimizationVariable> {
    static Gen<babBase::OptimizationVariable> arbitrary()
    {
        return gen::construct<babBase::OptimizationVariable>(
            gen::arbitrary<babBase::Bounds>(),
            gen::weightedElement<babBase::enums::VT>({{2, babBase::enums::VT_BINARY}, {16, babBase::enums::VT_CONTINUOUS}, {1, babBase::enums::VT_INTEGER}}),
            gen::weightedElement<bool>({{1, false}, {128, true}}),
            gen::arbitrary<std::string>());
    }
};
//enable random generation of BabNodes
template <>
struct Arbitrary<babBase::BabNode> {
    //arbitrary
    static Gen<babBase::BabNode> arbitrary()
    {
        return gen::construct<babBase::BabNode>(gen::arbitrary<double>(),
                                                gen::arbitrary<std::vector<babBase::OptimizationVariable>>(),
                                                gen::nonNegative<unsigned>() /*data set id*/,
                                                gen::nonNegative<int>() /*id*/,
                                                gen::nonNegative<unsigned>() /*depth*/,
                                                gen::weightedElement<bool>({{64, false}, {1, true}}) /*augment data*/);
    }
    //with a fixed number of optimization variables
    static Gen<babBase::BabNode> sized(int size)
    {
        return gen::construct<babBase::BabNode>(gen::arbitrary<double>(),
                                                gen::container<std::vector<babBase::OptimizationVariable>>(size, rc::gen::arbitrary<babBase::OptimizationVariable>()),
                                                gen::nonNegative<unsigned>() /*data set id*/,
                                                gen::nonNegative<int>() /*id*/,
                                                gen::nonNegative<unsigned>() /*depth*/,
                                                gen::weightedElement<bool>({{64, false}, {1, true}}) /*augment data*/);
    }
};

// create vector of nodes with same number of opt. var
// use like:  rc::Arbitrary<std::vector<babBase::BabNode>>::sized(size);
template <>
struct Arbitrary<std::vector<babBase::BabNode>> {
    static Gen<std::vector<babBase::BabNode>> sized(int size)
    {
        return rc::gen::container<std::vector<babBase::BabNode>>(rc::Arbitrary<babBase::BabNode>::sized(size));
    }
};


struct NodesAndVariablesFixture {
    std::vector<babBase::BabNode> nodes;
    std::vector<babBase::OptimizationVariable> vars;
    int size;
};
std::ostream&
operator<<(std::ostream& stream, const NodesAndVariablesFixture& mt)
{
    for (const babBase::BabNode& node : mt.nodes) {
        stream << node;
    }
    for (const babBase::OptimizationVariable& var : mt.vars) {
        stream << var;
    }
    return stream;
}
//enable random generation of nodesAndVariables to make setup easier
template <>
struct Arbitrary<NodesAndVariablesFixture> {
    //returns a struct with a node and a vector of optimization variables so that the size of
    //the optimization variable vector fits with size of the bound vector in node
    //  consistency of bounds in nodes with bounds in optimization variables is not ensured!
    static Gen<NodesAndVariablesFixture> sized(int i)
    {
        return rc::gen::build<NodesAndVariablesFixture>(
            gen::set(&NodesAndVariablesFixture::nodes, rc::Arbitrary<std::vector<babBase::BabNode>>::sized(i)),
            gen::set(&NodesAndVariablesFixture::vars, rc::gen::container<std::vector<babBase::OptimizationVariable>>(i, rc::gen::arbitrary<babBase::OptimizationVariable>())));
    }
    //return a NodesAndVariableFixture with a size up to i
    static Gen<NodesAndVariablesFixture> maxSized(int i)
    {

        return gen::exec([]() {
            auto i       = *rc::gen::inRange(2, 15);
            auto fixture = *rc::Arbitrary<NodesAndVariablesFixture>::sized(i);
            return fixture;
        });
    }

    //arbitrary is fixed to a maximum of 15 optimization variables
    static Gen<NodesAndVariablesFixture> arbitrary()
    {
        return Arbitrary<NodesAndVariablesFixture>::maxSized(15);
    }
};


}    // namespace rc
#pragma once
#include "RapidCheckUtils.h"
#include "babException.h"
#include "babOptVar.h"
#include <numeric>

RC_BOOST_PROP(feasible_continous_bounds_means_feasible, ())
{
    auto bounds = *rc::gen::arbitrary<babBase::Bounds>();
    //we only generate consistent bounds per default
    RC_ASSERT(bounds.are_consistent());
    babBase::OptimizationVariable var(bounds);
    RC_ASSERT(bounds.are_consistent() == var.has_nonempty_host_set());
}

//Bounds of [1.2,1.6] should be infeasible for integer variables
RC_BOOST_PROP(detects_integer_infeasibility, ())
{
    auto bounds = *rc::gen::arbitrary<babBase::Bounds>();


    auto type = *rc::gen::weightedElement<babBase::enums::VT>({{1, babBase::enums::VT_INTEGER}, {2, babBase::enums::VT_BINARY}});

    //we only generate consistent bounds per default
    RC_ASSERT(bounds.are_consistent());
    babBase::OptimizationVariable var(bounds, type);

    //either the host set inside the integers is non empty or the variable has to declare it as empty
    RC_ASSERT(std::floor(bounds.upper) >= std::ceil(bounds.lower) || var.has_nonempty_host_set() == false);
    //if the host set is not empty this should also be declared
    if (type == babBase::enums::VT_BINARY) {
        if (bounds.lower > 0 && bounds.upper < 1) {
            RC_ASSERT(var.has_nonempty_host_set() == false);
        }
        else if (bounds.upper < 0 || bounds.lower > 1) {
            RC_ASSERT(var.has_nonempty_host_set() == false);
        }
    }
    RC_ASSERT(std::floor(bounds.upper) < std::ceil(bounds.lower) || var.has_nonempty_host_set() == true);
}

RC_BOOST_PROP(bounds_are_not_changed_for_continuous_variable, ())
{
    auto lower = *rc::gen::arbitrary<double>();
    auto upper = *rc::gen::arbitrary<double>();
    babBase::OptimizationVariable var(babBase::Bounds(lower, upper));
    RC_ASSERT(var.bounds_changed_from_user_input() == false);
}

RC_BOOST_PROP(user_bounds_are_not_changed, ())
{
    auto lower = *rc::gen::arbitrary<double>();
    auto upper = *rc::gen::arbitrary<double>();
    auto type  = *rc::gen::weightedElement<babBase::enums::VT>({{3, babBase::enums::VT_INTEGER}, {3, babBase::enums::VT_BINARY}, {1, babBase::enums::VT_CONTINUOUS}});
    babBase::OptimizationVariable var(babBase::Bounds(lower, upper), type);
    RC_ASSERT(var.get_user_lower_bound() == lower);
    RC_ASSERT(var.get_user_upper_bound() == upper);
}

BOOST_AUTO_TEST_CASE(binay_variable_can_be_constructed_without_bounds)
{

    //BOOST_CHECK_NO_THROW(babBase::OptimizationVariable(babBase::enums::VT_BINARY));
    babBase::OptimizationVariable var = babBase::OptimizationVariable(babBase::enums::VT_BINARY);
    BOOST_CHECK(var.get_upper_bound() == 1);
    BOOST_CHECK(var.get_lower_bound() == 0);
    BOOST_CHECK(var.has_nonempty_host_set() == true);
}

BOOST_AUTO_TEST_CASE(non_binary_throws_when_constructed_without_bounds)
{
    auto integer_enum   = babBase::enums::VT_INTEGER;
    auto continous_enum = babBase::enums::VT_CONTINUOUS;
    BOOST_CHECK_THROW(babBase::OptimizationVariable(integer_enum), babBase::BranchAndBoundBaseException);
    BOOST_CHECK_THROW(babBase::OptimizationVariable(continous_enum), babBase::BranchAndBoundBaseException);
}

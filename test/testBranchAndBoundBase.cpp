/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * @file testBranchAndBoundBase.cpp
 *
 **********************************************************************************/

//#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE "testBranchAndBoundBase"
#include <boost/test/unit_test.hpp>

#include "testsuiteBabTree.h"
#include "testsuiteBrancher.h"
#include "testsuiteOptVar.h"
#include "testsuiteRandomGeneration.h"